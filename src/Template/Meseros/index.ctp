<?php
/**
  * @var \App\View\AppView $this
  * @var \App\Model\Entity\Mesero[]|\Cake\Collection\CollectionInterface $meseros
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Mesero'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="meseros index large-9 medium-8 columns content">
    <h3><?= __('Meseros') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('dni') ?></th>
                <th scope="col"><?= $this->Paginator->sort('nombre') ?></th>
                <th scope="col"><?= $this->Paginator->sort('apellido') ?></th>
                <th scope="col"><?= $this->Paginator->sort('telefono') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($meseros as $mesero): ?>
            <tr>
                <td><?= $this->Number->format($mesero->id) ?></td>
                <td><?= h($mesero->dni) ?></td>
                <td><?= h($mesero->nombre) ?></td>
                <td><?= h($mesero->apellido) ?></td>
                <td><?= h($mesero->telefono) ?></td>
                <td><?= h($mesero->created) ?></td>
                <td><?= h($mesero->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $mesero->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $mesero->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $mesero->id], ['confirm' => __('Are you sure you want to delete # {0}?', $mesero->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
